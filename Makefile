BIN_DIR = bin
CC = gcc
CFLAGS = -std=c99 -O3 -Wall -Wpedantic
FILE = http-server.c
EXE = image_tagger

all: mkbin $(EXE)

$(EXE): $(FILE)
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$(EXE) $<

.PHONY: clean mkbin

clean:
	rm -rf $(BIN_DIR)

mkbin:
	mkdir -p bin
